package com.mvmlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookingMeasureConverterApplication {

    public static void main(String[] args) {
        SpringApplication.run(CookingMeasureConverterApplication.class, args);
    }
}
