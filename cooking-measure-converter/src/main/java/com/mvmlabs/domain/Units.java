package com.mvmlabs.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Units {
	NO_UNITS(""), KILOGRAM("kg"), GRAM("g"), POUND("lb"), OUNCE("oz"), PINT("pint"), LITER("l"), MILLILITER("ml");

	private String value;

	private Units(final String abbreviation) {
		this.value = abbreviation;
	}

	@JsonValue
	public String getValue() {
		return value;
	}
}
