package com.mvmlabs.service;

import com.mvmlabs.domain.Measure;

public interface ConversionService {

	Measure kilosToPounds(Measure source);

	Measure litersToPints(Measure source);

	Measure poundsToKilos(Measure source);

	Measure pintsToLiters(Measure source);

}
