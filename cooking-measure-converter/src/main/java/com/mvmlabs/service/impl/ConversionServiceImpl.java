package com.mvmlabs.service.impl;

import org.springframework.stereotype.Service;

import com.mvmlabs.domain.Measure;
import com.mvmlabs.domain.Units;
import com.mvmlabs.service.ConversionService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ConversionServiceImpl implements ConversionService {

	private static final double LITERS_TO_PINTS = 1.75975D;
	private static final double KILOS_TO_POUNDS = 2.2D;

	@Override
	public Measure kilosToPounds(Measure source) {
		log.debug("Converting {} Kilos to Pounds", source);
		return Measure.builder().value(source.getValue() * KILOS_TO_POUNDS).units(Units.POUND).build();
	}

	@Override
	public Measure litersToPints(Measure source) {
		log.debug("Converting {} Litres to Pints", source);
		return Measure.builder().value(source.getValue() * LITERS_TO_PINTS).units(Units.PINT).build();
	}

	@Override
	public Measure poundsToKilos(Measure source) {
		log.debug("Converting {} Pounds to Kilos", source);
		return Measure.builder().value(source.getValue() / KILOS_TO_POUNDS).units(Units.KILOGRAM).build();
	}

	@Override
	public Measure pintsToLiters(Measure source) {
		log.debug("Converting {} Pints to Litres", source);
		double val = source.getValue() / LITERS_TO_PINTS;
		if (val < 1) {
			return Measure.builder().value(1000 * val).units(Units.MILLILITER).build();
		}
		return Measure.builder().value(val).units(Units.LITER).build();
	}

}
