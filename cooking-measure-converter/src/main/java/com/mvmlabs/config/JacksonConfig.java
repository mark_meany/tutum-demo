package com.mvmlabs.config;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializerProvider;

@Configuration
public class JacksonConfig {

	@Value("${app.service.dateformat}")
	private String dateFormatPattern;
	
	@Value("${app.service.numberformat}")
	private String numberFormatPattern;
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
		b.indentOutput(true).propertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES)
//				.dateFormat(new SimpleDateFormat(dateFormatPattern))
				.serializerByType(Double.class, new JsonSerializer<Double>() {
					@Override
					public void serialize(Double value, JsonGenerator jgen, SerializerProvider provider)
							throws IOException, JsonGenerationException {
						if (null == value) {
							jgen.writeNull();
						} else {
							final DecimalFormat myFormatter = new DecimalFormat(numberFormatPattern);
							final String output = myFormatter.format(value);
							jgen.writeNumber(output);
						}
					}
				});
		return b;
	}
}
