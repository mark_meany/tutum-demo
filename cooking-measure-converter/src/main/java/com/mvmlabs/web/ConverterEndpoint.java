package com.mvmlabs.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mvmlabs.domain.Measure;
import com.mvmlabs.domain.Units;
import com.mvmlabs.service.ConversionService;
import com.mvmlabs.web.domain.ConvertedResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ConverterEndpoint {

	@Autowired
	private ConversionService conversionService;
	
	@RequestMapping(value="/kg2lb/{kgs}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ConvertedResponse> convertKgToLb(@PathVariable("kgs") final double kgs) {
		log.debug("Sent {}.", kgs);
		final Measure source = Measure.builder().value(kgs).units(Units.KILOGRAM).build();
		final Measure converted = conversionService.kilosToPounds(source);
		return new ResponseEntity<ConvertedResponse>(ConvertedResponse.builder().source(source).converted(converted).timestamp(new Date()).build(), HttpStatus.OK);
	}

	@RequestMapping(value="/l2pint/{l}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ConvertedResponse> convertLitreToPints(@PathVariable("l") final double liters) {
		log.debug("Sent {}.", liters);
		final Measure source = Measure.builder().value(liters).units(Units.LITER).build();
		final Measure converted = conversionService.litersToPints(source);
		return new ResponseEntity<ConvertedResponse>(ConvertedResponse.builder().source(source).converted(converted).timestamp(new Date()).build(), HttpStatus.OK);
	}

	@RequestMapping(value="/lb2kg/{lbs}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ConvertedResponse> convertLbToKg(@PathVariable("lbs") final double lbs) {
		log.debug("Sent {}.", lbs);
		final Measure source = Measure.builder().value(lbs).units(Units.POUND).build();
		final Measure converted = conversionService.poundsToKilos(source);
		return new ResponseEntity<ConvertedResponse>(ConvertedResponse.builder().source(source).converted(converted).timestamp(new Date()).build(), HttpStatus.OK);
	}

	@RequestMapping(value="/pint2l/{pints}", method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ConvertedResponse> convertPintsToLitre(@PathVariable("pints") final double pints) {
		log.debug("Sent {}.", pints);
		final Measure source = Measure.builder().value(pints).units(Units.PINT).build();
		final Measure converted = conversionService.pintsToLiters(source);
		return new ResponseEntity<ConvertedResponse>(ConvertedResponse.builder().source(source).converted(converted).timestamp(new Date()).build(), HttpStatus.OK);
	}

}
