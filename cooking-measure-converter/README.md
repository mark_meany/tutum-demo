# Overview
A simple Micro Service for Tutum demonstration.

>> NOTE:: All paths shown below are relative to the project root folder, the one containing this file.

>> NOTE:: The Dockerfile does not do an update of the Linux system for brevity, it really should!

>> NOTE:: At the time of writing, Oct 2015, Tutum were providing a private Docker registry and I made use of that.

# Building it

	gradlew clean build

# Try it out

	java -jar build/libs/cooking-measure-converter-0.0.1-SNAPSHOT.jar

	curl -X GET -H "Accept: application/json" http://localhost:8085/kg2lb/1

I got the following:

	{
	  "timestamp" : 1446194351441,
	  "source" : {
	    "value" : 1.00,
	    "units" : "kg"
	  },
	  "converted" : {
	    "value" : 2.20,
	    "units" : "lb"
	  }
	}

# Remove the properties file
Now delete `src/main/resources/application.properties` as we never want to ship configuration with our applications.

	rm -f src/main/resources/application.properties
	gradlew clean build

## Try it out (1) - External Properties File

	java -jar build/libs/cooking-measure-converter-0.0.1-SNAPSHOT.jar --spring.config.location=`pwd`

## Try it out (2) - Environment Variables

	export LOGGING_LEVEL_COM_MVMLABS=DEBUG
	export SERVER_PORT=8085
	export APP_SERVICE_DATEFORMAT=dd/MM/yyyy HH:mm:ss
	export APP_SERVICE_NUMBERFORMAT=0.00
	java -jar build/libs/cooking-measure-converter-0.0.1-SNAPSHOT.jar

# Dockerise it

	docker login -e $TUTUM_EMAIL -u $TUTUM_USERNAME -p $TUTUM_PASSWORD tutum.co
	BUILD_NUMBER=`date +"%s"`
	docker build -t tutum.co/mmeany/cooking-measure-converter:${BUILD_NUMBER} .
    docker tag -f tutum.co/mmeany/cooking-measure-converter:${BUILD_NUMBER} tutum.co/mmeany/cooking-measure-converter:latest

	docker push tutum.co/mmeany/cooking-measure-converter

## Try it out (1) - Environment Variables

	docker run \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
	-e "APP_SERVICE_DATEFORMAT=dd/MM/yyyy HH:mm:ss" \
	-e "APP_SERVICE_NUMBERFORMAT=0.00" \
	-d \
	-p 8085:80 \
	tutum.co/mmeany/cooking-measure-converter

	curl -X GET -H "Accept: application/json" http://localhost:8085/kg2lb/1

	docker logs NNNN
	docker info NNNN
	docker stop NNNN
	docker rm NNNN
