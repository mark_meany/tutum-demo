# Overview
A simple Micro Service for Tutum demonstration.

This service depends on another service thats will also have been Dockerised. Please follow the instructions to Dockerise that service first.

>> NOTE:: All paths shown below are relative to the project root folder, the one containing this file.

>> NOTE:: The Dockerfile does not do an update of the Linux system for brevity, it really should!

>> NOTE:: At the time of writing, Oct 2015, Tutum were providing a provate Docker registry and I made use of that.

# Building it

	gradlew clean build

# Try it out
In order to test this service we need its partner to be running. Notice that this time we have given it a name, more on that later. This service is expected to be available for duration of this session:

	docker run \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
	-e "APP_SERVICE_DATEFORMAT=dd/MM/yyyy HH:mm:ss" \
	-e "APP_SERVICE_NUMBERFORMAT=0.00" \
	-d \
	--name cooking-measure-converter \
	-p 8085:80 \
	tutum.co/mmeany/cooking-measure-converter

	java -jar build/libs/recipe-conversion-api-0.0.1-SNAPSHOT.jar

	curl -X POST -H "Accept: application/json" \
	-H "Content-Type: application/json" \
	http://localhost:8086/convert \
	-d "{\"name\":\"Pancakes\",\"description\":\"A hearty pancake\",\"instructions\":\"Mix it all together. Whip till smooth. Cook it.\",\"feeds\":4,\"ingredients\":[{\"name\":\"Flour\",\"measure\":{\"value\":1.5,\"units\":\"kg\"}},{\"name\":\"Eggs\",\"measure\":{\"value\":30.0,\"units\":\"\"}},{\"name\":\"Water\",\"measure\":{\"value\":0.9,\"units\":\"l\"}}]}"

I got the following:

	{
	  "timestamp" : 1446197796243,
	  "recipe" : {
	    "name" : "Pancakes",
	    "description" : "A hearty pancake",
	    "instructions" : "Mix it all together. Whip till smooth. Cook it.",
	    "feeds" : 4,
	    "ingredients" : [ {
	      "name" : "Flour",
	      "measure" : {
	        "value" : 2.20,
	        "units" : "lb"
	      }
	    }, {
	      "name" : "Eggs",
	      "measure" : {
	        "value" : 30.00,
	        "units" : ""
	      }
	    }, {
	      "name" : "Water",
	      "measure" : {
	        "value" : 0.00,
	        "units" : "pint"
	      }
	    } ]
	  }
	}

So far so good then, our SpringBoot app is talking to its partner that is running in Docker on our PC.

# Remove the properties file
Now delete `src/main/resources/application.properties` as we never want to ship configuration with our applications.

	rm -f src/main/resources/application.properties
	gradlew clean build

## Try it out (1) - External Properties File

	java -jar build/libs/recipe-conversion-api-0.0.1-SNAPSHOT.jar --spring.config.location=`pwd`

## Try it out (2) - Environment Variables

	export SERVER.PORT=8086
	export LOGGING.LEVEL.COM.MVMLABS=DEBUG
	export APP.SERVICE.DATEFORMAT=dd/MM/yyyy HH:mm:ss
	export APP.SERVICE.NUMBERFORMAT=0.00
	export APP.DEBUG.REQUESTS=true
	export APP.SERVICE.URL=http://localhost:8085
	export APP.SERVICE.ENDPOINT.KG.TO.POUND=/kg2lb
	export APP.SERVICE.ENDPOINT.POUND.TO.KG=/lb2kg
	export APP.SERVICE.ENDPOINT.LITER.TO.PINT=/l2pint
	export APP.SERVICE.ENDPOINT.PINT.TO.LITER=/pint2l
	java -jar build/libs/recipe-conversion-api-0.0.1-SNAPSHOT.jar

# Dockerise it

	docker login -e $TUTUM_EMAIL -u $TUTUM_USERNAME -p $TUTUM_PASSWORD tutum.co
	BUILD_NUMBER=`date +"%s"`
	docker build -t tutum.co/mmeany/recipe-conversion-api:${BUILD_NUMBER} .
	docker tag -f tutum.co/mmeany/recipe-conversion-api:${BUILD_NUMBER} tutum.co/mmeany/recipe-conversion-api:latest
	docker push tutum.co/mmeany/recipe-conversion-api

## Try it out (1) - Environment Variables

	docker run \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
	-e "APP_SERVICE_DATEFORMAT=dd/MM/yyyy HH:mm:ss" \
	-e "APP_SERVICE_NUMBERFORMAT=0.00" \
	-e "APP.DEBUG.REQUESTS=true" \
	-e "APP.SERVICE.URL=http://cooking-measure-converter" \
	-e "APP.SERVICE.ENDPOINT.KG.TO.POUND=/kg2lb" \
	-e "APP.SERVICE.ENDPOINT.POUND.TO.KG=/lb2kg" \
	-e "APP.SERVICE.ENDPOINT.LITER.TO.PINT=/l2pint" \
	-e "APP.SERVICE.ENDPOINT.PINT.TO.LITER=/pint2l" \
	-d \
	--link cooking-measure-converter \
	-p 8086:80 \
	tutum.co/mmeany/recipe-conversion-api

	curl -X POST -H "Accept: application/json" \
	-H "Content-Type: application/json" \
	http://localhost:8086/convert \
	-d "{\"name\":\"Pancakes\",\"description\":\"A hearty pancake\",\"instructions\":\"Mix it all together. Whip till smooth. Cook it.\",\"feeds\":4,\"ingredients\":[{\"name\":\"Flour\",\"measure\":{\"value\":1.5,\"units\":\"kg\"}},{\"name\":\"Eggs\",\"measure\":{\"value\":30.0,\"units\":\"\"}},{\"name\":\"Water\",\"measure\":{\"value\":0.9,\"units\":\"l\"}}]}"

	docker logs NNNN
	docker info NNNN
	docker stop NNNN
	docker rm NNNN
