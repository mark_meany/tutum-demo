package com.mvmlabs.service;

import com.mvmlabs.domain.Recipe;

public interface CookingMeasureConversionService {

	Recipe convert(Recipe recipe);
	
}
