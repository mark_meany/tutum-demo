package com.mvmlabs.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mvmlabs.domain.Ingredient;
import com.mvmlabs.domain.Measure;
import com.mvmlabs.domain.Recipe;
import com.mvmlabs.domain.Units;
import com.mvmlabs.service.CookingMeasureConversionService;
import com.mvmlabs.web.domain.ConvertedMeasureResponse;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CookingMeasureConversionServiceRestImpl implements CookingMeasureConversionService {

	@Value("${app.service.url}")
	private String serviceUrl;
	@Value("${app.service.endpoint.kg.to.pound}")
	private String kgToLbEndpoint;
	@Value("${app.service.endpoint.pound.to.kg}")
	private String lbTokgEndpoint;
	@Value("${app.service.endpoint.liter.to.pint}")
	private String lToPintEndpoint;
	@Value("${app.service.endpoint.pint.to.liter}")
	private String pintTolEndpoint;

	@Autowired
	private RestTemplate restTemplate;
	
	private Map<Units, String> converters = new HashMap<>();

	@PostConstruct
	public void init() {
		converters.put(Units.KILOGRAM, kgToLbEndpoint);
		converters.put(Units.POUND, lbTokgEndpoint);
		converters.put(Units.LITER, lToPintEndpoint);
		converters.put(Units.PINT, pintTolEndpoint);
	}

	@Override
	public Recipe convert(Recipe recipe) {

		val converted = Recipe.builder()
				.name(recipe.getName())
				.description(recipe.getDescription())
				.instructions(recipe.getInstructions())
				.feeds(recipe.getFeeds()).build();

		for (val ingredient : recipe.getIngredients()) {
			converted.add(convert(ingredient));
		}
		return converted;
	}

	private Ingredient convert(final Ingredient ingredient) {
		val url = formEndpointUrl(ingredient.getMeasure());
		if (url == null) {
			return ingredient;
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		HttpEntity<?> entity = new HttpEntity<>(headers);

		ResponseEntity<ConvertedMeasureResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, ConvertedMeasureResponse.class);
//		val convertedMeasureResponse = restTemplate.getForObject(url, ConvertedMeasureResponse.class);
		return Ingredient.builder()
				.name(ingredient.getName())
				.measure(response.getBody().getConverted())
				.build();
	}

	private String formEndpointUrl(final Measure measure) {
		String url = null;
		if (converters.containsKey(measure.getUnits())) {
			url = String.format("%s%s/%f", serviceUrl, converters.get(measure.getUnits()), measure.getValue());
		}
		log.debug("Formed endpoint url: {}", url);
		return url;
	}
}
