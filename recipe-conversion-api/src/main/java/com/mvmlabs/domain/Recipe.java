package com.mvmlabs.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Recipe {
	private String name;
	private String description;
	private String instructions;
	private Integer feeds;
	private List<Ingredient> ingredients;
	
	public List<Ingredient> getIngredients() {
		if (ingredients == null) {
			ingredients = new ArrayList<>();
		}
		return ingredients;
	}
	
	public Recipe add(final Ingredient ingredient) {
		getIngredients().add(ingredient);
		return this;
	}
}
