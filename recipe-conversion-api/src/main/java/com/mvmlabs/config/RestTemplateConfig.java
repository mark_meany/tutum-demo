package com.mvmlabs.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import lombok.val;

@Configuration
public class RestTemplateConfig {

	@Value("${app.debug.requests}")
	private boolean debugRequests;
	
	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();

		ClientHttpRequestInterceptor ri = configureRequestLogger();
		List<ClientHttpRequestInterceptor> ris = new ArrayList<>();
		ris.add(ri);
		restTemplate.setInterceptors(ris);
		restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));

		return restTemplate;
	}

	private ClientHttpRequestInterceptor configureRequestLogger() {
		
		return new ClientHttpRequestInterceptor() {

			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {

		        ClientHttpResponse response = execution.execute(request, body);

		        log(request,body,response);

		        return response;
			}

			private void log(HttpRequest request, byte[] body, ClientHttpResponse response) {
				if (debugRequests) {
					val retval = new StringBuilder();
					try {
						val in = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
						String line = "";
						while ((line = in.readLine()) != null) {
							retval.append(line).append('\n');
						}

					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					System.out.println(new String(retval));
				}
			}

		};
	}
}
