package com.mvmlabs.web.domain;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.mvmlabs.domain.Measure;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@XmlRootElement
public class ConvertedMeasureResponse {
	private Date timestamp;
	private Measure source;
	private Measure converted;
}