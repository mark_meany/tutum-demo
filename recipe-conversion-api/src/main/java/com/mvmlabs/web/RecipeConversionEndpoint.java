package com.mvmlabs.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mvmlabs.domain.Recipe;
import com.mvmlabs.service.CookingMeasureConversionService;
import com.mvmlabs.web.domain.ConversionResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class RecipeConversionEndpoint {

	@Autowired
	private CookingMeasureConversionService cookingMeasureConversionService;

	@RequestMapping(value="/convert", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ConversionResponse> convertUnits(@RequestBody final Recipe recipe) {
		log.debug("Converting units for {} recipe.", recipe.getName());
		
		return new ResponseEntity<>(ConversionResponse.builder()
				.timestamp(new Date())
				.recipe(cookingMeasureConversionService.convert(recipe))
				.build(), HttpStatus.OK);
	}
}
