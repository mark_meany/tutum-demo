package com.mvmlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecipeConversionApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecipeConversionApiApplication.class, args);
    }
}
