package com.mvmlabs;

import java.util.ArrayList;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mvmlabs.domain.Ingredient;
import com.mvmlabs.domain.Recipe;
import com.mvmlabs.domain.Units;

import lombok.val;

public class Poi {

	@Test
	public void tryit() throws Exception {
		val ingredients = new ArrayList<Ingredient>();
		ingredients.add(new Ingredient("Flour", 1.5D, Units.KILOGRAM));
		ingredients.add(new Ingredient("Eggs", 30D, Units.NO_UNITS));
		ingredients.add(new Ingredient("Water", .9D, Units.LITER));
		Recipe r = Recipe.builder()
				.name("Pancakes")
				.description("A hearty pancake")
				.instructions("Mix it all together. Whip till smooth. Cook it.")
				.feeds(4)
				.ingredients(ingredients)
				.build();
		ObjectMapper om = new ObjectMapper();
		System.out.println(om.writeValueAsString(r));
	}
	
	
}
