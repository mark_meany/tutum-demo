# Overview
Tutum is a third party service provider that manages the deployment of Docker images to Docker hosts or Nodes as Tutum refer to them.

Using Tutum requires the following

* An AWS Account
* A suitable IAM user for use with Tutum
* A Tutum Account
* Defining a cluster of Nodes in Tutum.
* Defining an application stack and loading it into Tutum
* Deploying the application stack to the node cluster.

Be aware that Tutum allows nodes to be tagged. Within a stack file an image can identify what nodes are deployment candidates by listing appropriate tags.

HA-Proxy

Weave
