# Overview
This demonstration covers writing two simple Micro Services using Spring Boot and taking them through the process of migrating to Docker, using Spring Configuration server for storing encrypted application properties, running a stack locally and deploying the same stack to Tutum where HA-Proxy and Weave Works Weave will be provide load balancing, DNS and service discovery to multiple instances of each service deployed across a number of Amazon AWS nodes.

# The Micro Services
Each of the Micro Services are documented in their respective projects, however it is worth noting a few things you should be able to take away from them:

* Spring Config Client has been included in each, this adds a little to the bootstrap time on application start-up but allows the application to read from an additional property source if present: Spring Config server.
* Project Lombok that simplified your Java code no end.
* Customising Jackson JSON generation.
* Logging requests received by Spring RestTemplate.
* Out of the box, Spring Boot supports both JSON and XML
* Creating a Docker image containing the MicroService
* Various means of providing application configuration to the application when running the Docker image.

# How to read through this demonstration

* First take a look and follow along to the steps in the [cooking-measure-converter](cooking-measure-converter/README.md) project.
* Next take a look and work through the steps in the [recipe-conversion-api](recipe-conversion-api/README.md) project.
* Once you have both of these Micro Services available as Docker images, take a look at [spring-config-server](spring-config-server/README.md) to learn how to separate Configuration from the application. By this stage you will have three Docker images available to you and be able to run the stack locally on your PC.
* The final step is the move to the [Amazon Cloud via Tutum](tutum/README.md). This step explains how to define an application stack, link an AWS account to Tutum, launch a cluster of nodes in AWS EC2 and then deploy the application stack to that cluster of nodes.

# Where next?

* RDS Database instances
* Cloud Formation
* Centralised Logging
* Centralised monitoring
* API Management Platform
