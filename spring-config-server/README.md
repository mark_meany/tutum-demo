# Overview
Spring provide a config server that can read application properties from a GitHub repository. Further, the properties can be encrypted and the server will perform decryption of them before passing to a client.


>> NOTE:: All paths shown below are relative to the project root folder, the one containing this file.

>> NOTE:: The Dockerfile does not do an update of the Linux system for brevity, it really should!

>> NOTE:: At the time of writing, Oct 2015, Tutum were providing a provate Docker registry and I made use of that.

>> NOTE:: For simplicity the server has been configured to use local file system and not GitHub. Don't do this for real. When more time is available the demo may be extended to cover a fully functioning server.

# Building it

	gradlew clean build

# Try it out

	curl -X GET -H "Accept: application/json" http://localhost:8888/cooking-measure-converter/dev/demo

# Dockerise it
Take a look at the Dockerfile, it copies a config directory onto the Docker Image. This directory contains a couple of configuration files for the two Micro Services and are identified by their location and name.

	docker login -e $TUTUM_EMAIL -u $TUTUM_USERNAME -p $TUTUM_PASSWORD tutum.co
	BUILD_NUMBER=`date +"%s"`
	docker build -t tutum.co/mmeany/spring-config-server:${BUILD_NUMBER} .
	docker tag -f tutum.co/mmeany/spring-config-server:${BUILD_NUMBER} tutum.co/mmeany/spring-config-server:latest
	docker push tutum.co/mmeany/spring-config-server

# Try it out

	docker run \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
	-d \
	--name spring-config-server \
	-p 8888:80 \
	tutum.co/mmeany/spring-config-server

	curl -X GET -H "Accept: application/json" http://localhost:8888/cooking-measure-converter/dev/demo

## Run the rest of the stack
We can run the entire stack now. Because the Micro Services were build with Spring Config Client they can read properties from a Config Server during bootstrap. The address of the config server is passed as a parameter much like any other:

	docker run \
	-e "SPRING_APPLICATION_NAME=cooking-measure-converter" \
	-e "SPRING_PROFILES_ACTIVE=test" \
	-e "SPRING_CLOUD_CONFIG_URI=http://spring-config-server" \
	-e "SPRING_CLOUD_CONFIG_LABEL=demo" \
	-e "SPRING_CLOUD_CONFIG_FAILFAST=true" \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
    --name cooking-measure-converter \
	--link spring-config-server \
	-d \
	-P \
	tutum.co/mmeany/cooking-measure-converter

	docker run \
	-e "SPRING_APPLICATION_NAME=recipe-conversion-api" \
	-e "SPRING_PROFILES_ACTIVE=test" \
	-e "SPRING_CLOUD_CONFIG_URI=http://spring-config-server" \
	-e "SPRING_CLOUD_CONFIG_LABEL=demo" \
	-e "SPRING_CLOUD_CONFIG_FAILFAST=true" \
	-e "SERVER_PORT=80" \
	-e "LOGGING_LEVEL_COM_MVMLABS=DEBUG" \
	-d \
    --name recipe-conversion-api \
	--link spring-config-server \
	--link cooking-measure-converter \
	-p 8086:80 \
	tutum.co/mmeany/recipe-conversion-api

## Try it out

	curl -X POST -H "Accept: application/json" \
	-H "Content-Type: application/json" \
	http://localhost:8086/convert \
	-d "{\"name\":\"Pancakes\",\"description\":\"A hearty pancake\",\"instructions\":\"Mix it all together. Whip till smooth. Cook it.\",\"feeds\":4,\"ingredients\":[{\"name\":\"Flour\",\"measure\":{\"value\":1.5,\"units\":\"kg\"}},{\"name\":\"Eggs\",\"measure\":{\"value\":30.0,\"units\":\"\"}},{\"name\":\"Water\",\"measure\":{\"value\":0.9,\"units\":\"l\"}}]}"
